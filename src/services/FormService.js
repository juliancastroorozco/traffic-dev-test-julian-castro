import axios from 'axios';
/**
 * 
 * @param {Object} payload 
 * @returns {Promise}
 */
function save(payload){
    try{
        const url = 'https://formspree.io/f/mzbknedq';
        return axios.post(url, payload);
    }catch(e){
        return e;
    }
}
/**
 * Count the number of checkboxes checked 
 * @param {Object} formData 
 * @returns {string}
 */
function checkCount(formData) {
    const totalChecks = formData.values.reduce((accumulator, currentValue) => accumulator + currentValue);
    return totalChecks;
}
/**
 *Check if the checkboxes are in the right range, otherwise reutrn error message
 * @param {Object} formData 
 * @returns {string}
 */
function getError(formData) {
    const totalChecks = checkCount(formData);
    if(totalChecks < 2){
        return 'Please select 2 or more services';
    }
    if(totalChecks > 5){
        return 'Please select 5 or fewer services';
    }
    return '';
}
const formService =  {
    save,
    checkCount,
    getError 
};
export default formService;