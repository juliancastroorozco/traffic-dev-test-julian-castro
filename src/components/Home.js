import { Fade  } from 'react-slideshow-image';
import "react-slideshow-image/dist/styles.css";
import arrowLeft from '../img/ArrowLeft.png';
import arrowRight from '../img/ArrowRight.png';
import './Home.css';
import Form from "./Form";

  function Home() {
    const fadeProperties = {
      duration: 5000,
      transitionDuration: 500,
      infinite: true,
      indicators: false,
      prevArrow: <button className="Home-prev nav default-nav "><img src={arrowLeft} alt="Previous" /></button>,
      nextArrow: <button className="Home-next nav default-nav "><img src={arrowRight} alt="Next" /></button>
    }
    return (
      <div className="Home-main"> 
        <div className="Home-carousel">
          <Fade {...fadeProperties}>
            <div className="Home-slide each-fade">
              <h2>Two stunning new Townhome Releases Launching Early 2021</h2>
            </div>
            <div className="Home-slide each-fade">
              <h2>Two stunning new Townhome Releases Launching Early 2022</h2>
            </div>
            <div className="Home-slide each-fade">
              <h2>Two stunning new Townhome Releases Launching Early 2023</h2>
            </div>
          </Fade>
          <a className="Home-register" href="#register">Register</a>
        </div>
        <Form id="register" />
      </div>
    );
}

export default Home;
