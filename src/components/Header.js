import './Header.css';

function Header() {
  return (
    <header className="Header-main">
      <div className="Header-top-bar">
        <div className="Header-logo"/>
      </div>
      <div className="Header-mid-bar">
        <a
          className="Header-link"
          href="#map"
          target="_blank"
        >
          <svg width="15" height="20" viewBox="0 0 15 20" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path fillRule="evenodd" clipRule="evenodd" d="M13.08 6.1599C13.08 2.9823 10.4976 0.399902 7.32 0.399902C4.1424 0.399902 1.56 2.9823 1.56 6.1599C1.56 10.4799 7.32 16.7199 7.32 16.7199C7.32 16.7199 13.08 10.4799 13.08 6.1599ZM5.40001 6.15989C5.40001 5.10389 6.26401 4.23989 7.32001 4.23989C8.37601 4.23989 9.24001 5.10389 9.24001 6.15989C9.24001 7.21589 8.38561 8.07989 7.32001 8.07989C6.26401 8.07989 5.40001 7.21589 5.40001 6.15989ZM0.600006 17.6799V19.5999H14.04V17.6799H0.600006Z" />
          </svg>
          <span className="Header-link-text">VISIT OUR SALES CENTRE</span>
        </a>
        <a
          className="Header-link"
          href="tel:1300-354-786"
        >
          <svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path fillRule="evenodd" clipRule="evenodd" d="M16.7067 12C15.5386 11.9972 14.3781 11.8128 13.2667 11.4533C12.9213 11.3385 12.5406 11.4259 12.28 11.68L10.16 13.8C7.42727 12.4078 5.20553 10.1861 3.81334 7.45333L5.93334 5.33333C6.19631 5.08905 6.29042 4.71263 6.17334 4.37333C5.8139 3.26194 5.62948 2.10141 5.62668 0.933333C5.61223 0.413582 5.18663 -0.000200563 4.66668 7.29328e-08H1.29334C0.76315 7.29328e-08 0.333344 0.429807 0.333344 0.96C0.333344 10.0027 7.66394 17.3333 16.7067 17.3333C17.2369 17.3333 17.6667 16.9035 17.6667 16.3733V13C17.6776 12.7385 17.5813 12.484 17.4 12.2952C17.2188 12.1064 16.9684 11.9998 16.7067 12Z" />
          </svg>
          <span className="Header-link-text">1300 354 786</span>
        </a>
      </div>
      <div className="Header-bottom-bar"></div>
    </header>
  );
}

export default Header;
