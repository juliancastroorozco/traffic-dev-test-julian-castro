import { useEffect, useState } from "react";
import formService from "../services/FormService";
import './Form.css';

function Form(props) {
  const initialFormData = {name:'',phone:'',email:'', location: '', values: [false,false,false,false,false,false,false,false] };
  const [formData, setFormData] = useState(initialFormData);
  const initialFormMessages = {showError: false, errorMessage: '', showMessage: false, message: '', formValid: true, formLoading: false}
  const [formMessages, setFormMessages] = useState(initialFormMessages);
  //get the initial selected values and error messages on first render
  useEffect(()=>{
    const savedFormData = {...initialFormData, ...(JSON.parse(localStorage.getItem('formData'))  || initialFormData)};//blend what is stored with the defaults
    const checkCount = formService.checkCount(savedFormData);
    if(checkCount >0){//if user previously select a value, show error if needed
      var newFormMessages = {...initialFormMessages};
      const newErrorMessage = formService.getError(savedFormData);
      if(newErrorMessage){//show error
        newFormMessages.showErrorMessage = true;
        newFormMessages.errorMessage = newErrorMessage;
        newFormMessages.formValid = false;
        setFormMessages(newFormMessages );
      }
    }
    setFormData(savedFormData);// eslint-disable-next-line
  },[]);
  const handleSubmit = (e) => {
    e.preventDefault();
    var newFormMessages = {...initialFormMessages};
    newFormMessages.formLoading = true;
    setFormMessages({...newFormMessages});
    formService.save(formData).then((response)=>{
      newFormMessages.formLoading = false;
      if(response.status === 200){
        newFormMessages.showMessage = true;
        newFormMessages.message = 'Thank you for your registration!';
      }else{
        newFormMessages.showErrorMessage = true;
        newFormMessages.errorMessage = 'Something went wrong, please check the form';
      }
      setFormMessages({...newFormMessages});
    }).catch((error) => {
      newFormMessages.formLoading = false;
      newFormMessages.showErrorMessage = true;
      newFormMessages.errorMessage = 'Something went wrong, please try again';
      setFormMessages({...newFormMessages});
    });
  }
  const handleFormChange = (newFormData) => {
    setFormData({...formData, ...newFormData});
  }
  /**
   * handle value change of the checkboxes
   * @param {number} index 
   * @param {boolean} newValue 
   */
  const handleValueChange = (index, newValue) => {
    var newFormMessages = {...initialFormMessages};
    formData.values[index] = newValue;
    setFormData({...formData});
    localStorage.setItem('formData', JSON.stringify({values: formData.values}))
    const newErrorMessage = formService.getError(formData);
    if(newErrorMessage){
      newFormMessages.showErrorMessage = true;
      newFormMessages.errorMessage = newErrorMessage;
      newFormMessages.formValid = false;
    }
    setFormMessages(newFormMessages );
  }  
  const handleFormReset = () => {
    handleFormChange(initialFormData)
    setFormMessages(initialFormMessages);
  }
  return (
    <div className="Form-container" id={props.id}>
      <h1>Be the first to register for new townhome releases for first option</h1>
      <form onSubmit={handleSubmit}>
        <div className="Form-main" >
          <input type="text" className="Form-input" onChange={(e)=> {handleFormChange({name:e.target.value})}} placeholder="First Name*" value={formData.name} required={true} />
          <input type="email" className="Form-input" onChange={(e)=> {handleFormChange({email:e.target.value})}} placeholder="Email*" value={formData.email} required={true} />
          <input type="text" className="Form-input" onChange={(e)=> {handleFormChange({phone:e.target.value})}} placeholder="Phone*" value={formData.phone} required={true} />
          <select className="Form-input" onChange={(e)=> {handleFormChange({location:e.target.value})}} value={formData.location}>
            <option value="">When are you looking to buy</option>
            <option value="Melbourne">Melbourne</option>
            <option value="Sydney">Sydney</option>
          </select>
          {formData.values.map((val, index)=>(
            <div key={index} className="Form-checkbox-container">
              <input type="checkbox" className="Form-checkbox" onChange={(e)=> {handleValueChange(index, e.target.checked)}} checked={val} /> <label className="Form-label">Value {index+1}</label>
            </div>
          ))}
        </div>
        <p className={"Form-message "+(formMessages.showErrorMessage? 'Form-message-alert': '')}>{formMessages.errorMessage || formMessages.message}&nbsp;</p>
        <div className="Form-submit-container">
          <input type="submit" className="Form-submit" disabled={!formMessages.formValid || formMessages.formLoading} value="SUBMIT"/>
          <input type="reset" className="Form-reset" onClick={handleFormReset}  value="RESET"/>
        </div>
      </form>
    </div>
  );
}

export default Form;
