# Traffic Dev Test

## Available Scripts

In the project directory, you can run:

### `npm install`
### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

## Notes

- Desktop version has a black arrow next to the car that is not present in mobile version (and no apparent use), omitted.
- Reset button is aligned with the form text inputs to make it look more organized.
- Mandatory text/select fields (besides logic for checkboxes) not mentioned in test but implied by the "*" that is present in the placeholder text of the mobile version but absent in the desktop one, made both versions consistent by using the placeholder text from mobile version, email is validated if any given.
- Image file names where left as is.
- Placeholder text color for text inputs was set (instead of the designs black color) to a light grey to help users tell apart between text from the place holder and text they have typed (UX).
- Register button scrolls down to the registration form.
- Tested in all mentioned browsers except safari mobile due to lack of access to a physical device and online testing tools like browserling don't have safari mobile.
- No errors/warnings from https://validator.w3.org/. 

**Any changes to the original design in a real production environment would have been discussed with the designer team and only do them if they agree**